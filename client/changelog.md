### Alpha Notice
This version still requires testing, especially for backports.
If you encounter any issues, please report them on Discord: https://discord.gg/uueEqzwCJJ.

Versions 2.0.x and 2.1.x are protocol-compatible,
so there’s no need to worry if the server hasn't been updated to 2.1.x.

### Changes in 2.1.3
- Scissor zone of tab content is moved two "pixels" up. Fixes white line in 1.21.2+ under the tab menu. (https://imgur.com/a/FJQGNpo)